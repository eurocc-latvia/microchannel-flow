#!/bin/bash
### Job name
#PBS -N Microchannels
### Project code
#PBS -A eurocc
#PBS -q batch
### Select 1 node with 24 cores and amount of memory per core
#PBS -l nodes=1:ppn=24,pmem=4g,feature=vasara
### Time limit, hrs:min:sec
#PBS -l walltime=36:00:00
### Merge output and error files
#PBS -j oe


# User-defined initial values
CH_nr=6             # number of channels 3, 4, 6 or 8
U_INIT=0.3196       # inlet velocity [m/s]
Q_INIT=250          # the heat flux [W/m^2]
T_INIT=300          # initial temperature [K]
NUMBER_OF_SUBDOMAINS=24     # number of cores

# Location of the case
PBS_DIR1=foam
PBS_DIR2=channels
PBS_DIR3=channels

echo "------------------------------------------------------------------------------"
echo "--- Starting job at: `date`"
echo "--- Node:`hostname`"
echo

echo -e "  - Nr of channels: $CH_nr"


# Prepare environment
module load openfoam/openfoam-5.0-sysmpi


echo -e "\n  Pre-processing"
#------------------------------------------------------------------------------
# Create case dir
echo -e "  - Create case dir"
JOBDIR=~/$PBS_JOBNAME$CH_nr-$PBS_JOBID
mkdir $JOBDIR; cd $JOBDIR
echo " - Current directory: $JOBDIR"

# Copy project files to case dir
cp -a ~/$PBS_DIR1/$PBS_DIR2/$PBS_DIR3$CH_nr/* $JOBDIR

#------------------------------------------------------------------------------
echo -e "  - Change simulation parameters"
# decomposeParDict
sed \
    -e s/"\(numberOfSubdomains[ \t]*\) NUMBER_OF_SUBDOMAINS;"/"\1 $NUMBER_OF_SUBDOMAINS;"/g \
    decomposeParDict > temp.$$
    mv temp.$$ system/decomposeParDict

for ((nr=1; nr<= $CH_nr; nr++))
do
# U
sed \
    -e s/"\(internalField uniform ([ \t]*\) U_INIT 0 0);"/"\1 $U_INIT 0 0);"/g \
    fluid/U > temp.$$
    mv temp.$$ 0/ch$nr/U

# T fluid
sed \
    -e s/"\(internalField uniform[ \t]*\) T_INIT;"/"\1 $T_INIT;"/g \
    fluid/T > temp.$$
    mv temp.$$ 0/ch$nr/T
done

# T solid
sed \
    -e s/"\(internalField uniform[ \t]*\) T_INIT;"/"\1 $T_INIT;"/g \
    -e s/"\(q uniform[ \t]*\) Q_INIT;"/"\1 $Q_INIT;"/g \
    solid/T > temp.$$
    mv temp.$$ 0/domain0/T

#------------------------------------------------------------------------------
echo "  - Create a file with initial values"
# readMe
sed \
    -e s/"\(Uin=[ \t]*\) U_INIT"/"\1 $U_INIT"/g \
    -e s/"\(q=[ \t]*\) Q_INIT"/"\1 $Q_INIT"/g \
    -e s/"\(Tin=[ \t]*\) T_INIT"/"\1 $T_INIT"/g \
    initialValues > temp.$$
    mv temp.$$ info/initialValues

# Pre-processing
./preProc
# Run OpenFOAM solver
./simul
# Post-processing
./postProc

echo "--- Job finished at: `date`"
echo "------------------------------------------------------------------------------"
