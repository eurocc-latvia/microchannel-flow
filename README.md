# Summary

This example demonstrates the ability of high-performance computing (HPC) to solve complex numerical problems related to fluid and heat transfer using computational fluid dynamics (CFD). The model describes an aluminium panel that is actively cooled by a liquid using microchannels. Channels with a rectangular cross-section of small hydraulic diameter have been applied. By changing the initial parameters, the model can be modified to obtain the case when the panel is heated using a liquid flow.

The demonstration model is designed to run on [OpenFOAM](https://openfoam.org) (version 5.x). The simulation case contains all the necessary OpenFOAM files, scripts for running the model, including pre-processing and post-processing operations. 
Using the built-in capabilities of OpenFOAM, the creation of a computational domain and mesh, as well as parallelization of the simulation case is ensured.

The target audience is anyone interested in a numerical solution to a coupled problem that takes into account heat transfer in both liquids and solids, including industry and research communities related to biomimicry, researchers and the technical subject students in higher education institutions interested in CFD simulations.

**This HPC demonstration was prepared as a part of the training and dissemination activities organized by the [EuroCC National HPC Competence Centre of Latvia](https://eurocc-latvia.lv/?lang=en).**

<img src="README_fig/graphicalAbstract.jpg" width="600">


# Table of Contents
- [ Problem Background ](#back)
- [ Contents of the Case](#list)
- [ Running the Case](#run)
    -   [Getting the Case and Preparing the Environment](#init)
    -   [Pre-pocessing of the Case](#pre)
    -   [Simulations](#sim)
    -   [Post-processing and Visualization](#post)
- [ Model Assumptions ](#limit)
- [ References ](#ref)


# Problem Background <a name="back"></a>

Bioinspired vascular networks are widely applicable in engineering, for example, for battery cooling [[1](#ref)], in nanosatellite panels [[2](#ref)] as well in phase change materials [[3](#ref)] or self-healing and self-cooling materials [[4](#ref)]. This example demonstrates the methodology of a three-dimensional CFD simulations of microvascular channels in a plate. The scenario is inspired by Pety et al. [[5](#ref)].

The numerical model has solid (the plate) and liquid (microchannels) domains that are connected through a specially defined interface between the both regions. The fluid enters microchannels with a constant inlet velocity _U_ [m/s] and temperature _T_ [K]. A coolant is defined as a Newtonian fluid. Based on the [Reynolds number](https://en.wikipedia.org/wiki/Reynolds_number), which is calculated using the height of the microchannel as the characteristic linear dimension, the flow is laminar. The finite volume discretization of Navier-Stokes equations (the momentum and mass conservation equations) is applied to solve the fluid flow in the microchannels. The pressure-momentum coupling is provided by SIMPLE algorithm. A constant heat flow _q_ [W/m<sup>2</sup>] is applied to the upper and lower surfaces of the plate, which ensures heating of the plate. The side walls are insulated. The plate temperature is the main outcome of simulations. The coupled simulations are performed by a conjugate heat transfer solver.
More information about governing equations, SIMPLE algorithm and their implementation in OpenFOAM can be found in the [Mathematics,  Numerics,  Derivations  and  OpenFOAM(R)](https://holzmann-cfd.com/community/publications/mathematics-numerics-derivations-and-openfoam) by Tobias  Holzmann.

In the example, there are four cases with different number of microchannels in the plate - 3, 4, 6 or 8. The coolant flows in _x_-direction.  Model schemes are shown below. All dimensions are in milimeters. 

 <img src="README_fig/models.jpg" width="800">

The inlet velocity **_Uinit_**, temperature **_Tinit_** and heat flow **_qinit_** are introduced into the model as variables that can be modified by the end user. _When changing the initial conditions, be careful not to go out of the model scope, see [below](#limit)._

The time required for the simulation, depending on the number of cores used, is shown in the figure below.

<img src="README_fig/HPCvsPC.jpg" width="300">

# Contents of the Case <a name="list"></a>

An overview of all the files in this repository is below. You do not need to understand the meaning of each file in detail to run this simulation.

- `channels` - OpenFOAM simulation files

    - `channels*` - Four simulation cases with different number of microchannels, instead of <mark>'*'</mark> is the number of channels (3, 4, 6 and 8)

        - `0` - Initial and boundary conditions for both solid and fluid regions
            - `ch*` - Conditions for each microchannel
            - `domain0` - Conditions for the panel
        - `constant` - In this folder is placed all properties that are preserved during execution - the mesh, thermophysical, flow and region properties for both solid and fluid domains
        - `fluid` - The file templates to define initial values for flow velocity and temperature of channels
        - `info` - Summary of user-defined velocity, temperature and panel heat flow
        - `solid` - A file template for initial values of the plate temperature
        - `system` -  The files that configure or help configure the case - a definition of solver, discretization schemes, geometry, parallelization and other properties to obtain OpenFOAM solution.
        - `simul` - This script is the one that takes care of running the solver 
        - `decomposeParDict` - A file template that provides parallelization
        - `initialValues` - A file template that will compile a user-defined inlet flow velocity, temperature, and panel heat flow
        - `postProc` - This script contains commands for post-processing
        - `preProc` - This script contains commands for pre-processing
        - `Residuals` - This script create _gnuplot_ graph of residuals using log file
        - `Temperature` - This script create _gnuplot_ graph of the minimum and maximum temperature in the computational domain using log file
    - `.color` - Definition of log file colors 
    - `.preamble` - log file preamble definition
    - `run-simulation` - Script to run OpenFOAM simulation without job management system
- `run-simulationPBS.sh` - Script to run OpenFOAM simulation with the Portable Batch System
- `README.md` - This readme file
- `README_fig` - Images for this readme file

# Running the Case <a name="run"></a>

Simulation examples are designed so that pre-processing and simulations can be performed using one software - **[OpenFOAM 5.x](https://openfoam.org)**. Visualization of results can be done using **[Paraview](https://www.paraview.org/)** and **[gnuplot](http://www.gnuplot.info/)**.

Below are instructions to run the simulation without root rights. 

## Getting the Case and Preparing the Environment <a name="init"></a>

Download the case directly from GitLab by executing the command below or download it as an archive and copy it to the HPC cluster.

```
git clone https://gitlab.com/eurocc-latvia/microchannel-flow.git
```

To save computing resources and time, the whole computing process is divided into three stages - pre-processing, simulation, post-processing.
 Go to the appropriate `channels*` folder to create permission to run the corresponding script files and follow these steps

```
chmod +x simmul

chmod +x preProc

chmod +x postProc

chmod +x Residual
```

## Pre-processing of the Case <a name="pre"></a>
Depending on the predefined cluster environment, there are two options for running the simulation with the appropriate scripts. 
If the cluster uses the Portable Batch System (PBS),  `run-simulationPBS.sh`  must be used. _This script is adapted to run on [Riga Technical University HPC](https://hpc.rtu.lv/?lang=en) cluster._ If no workload or job management system is used, run the `run-simulation` script. 

### User-Defined Initial Values
In both scripts, the user can specify the number of microchannels, the inlet flow velocity and the initial temperature, the panel heat flow, and the number of cores.

```
# User-defined initial values
CH_nr=6             # number of channels 3, 4, 6 or 8
U_INIT=0.3196       # inlet velocity [m/s]
Q_INIT=250          # the heat flux [W/m^2]
T_INIT=300          # initial temperature [K]
NUMBER_OF_SUBDOMAINS=24     # number of cores
```

The necessary computational resources in the `run-simulationPBS.sh` must also be indicated in the header part of PBS script. On Riga Technical University HPC cluster it would be:
```
#PBS -l nodes=1:ppn=24,pmem=4g,feature=rudens
```

The user must also specify the location of the case directory in these script files.

```
# Location of the case
PBS_DIR1=foam
PBS_DIR2=channels
PBS_DIR3=channels

# Copy project files to case dir
cp -a ~/$PBS_DIR1/$PBS_DIR2/$PBS_DIR3$CH_nr/* $JOBDIR
```

When the directory location is specified, the script creates the files needed for the calculations with user-defined initial values ​​(U and T for fluid, T for solid, and a file for paralelization, and a file that summarizes the input values).

Material properties are defind in `constant/ch*/thermophysicalProperties` for the fluid and `constant/domain0/thermophysicalProperties` for the solid region.

### ./preProc

Running this script separately creates a computing domain, generates a mesh and divide the domain into solid and fluid regions. The locations of the microchannel regions are specified in the `system / topoSetDict`, the rest of the domain is defined as solid. 

Depending on the number of microchannels, the script may take more than an hour to complete. Therefore, it is recommended that you do this only the first time that a computing domain is generated, and do not run this script again when performing parametric studies.

## Simulations <a name="sim"></a>

If the required software is installed/loaded and no workload or job management system is present, go to the `channels` folder and run the `run-simulation` script by executing in the terminal:

```
./run-simulation **
```
where <mark>'**'</mark> is a user-selected folder ID.

If the computer or computing cluster uses the PBS, run the following command to submit the simulation

```
qsub run-simulationPBS.sh
```

Scripts execution provide a complete process - pre-processing, calculation part and post-processing. To be able to verify the execution of commands log files are generated.

Depending on the number of microchannels, the calculation part requires 12 to 34 hours using 24 cores. The convergence
criterion employed was for velocity and continuity residuals to reach 10<sup>-4</sup>. The convergence can be checked during the simulation by

```
./Residuals
```

The _gnuplot_ is required to display the graphs.


## Post-processing and Visualization <a name="post"></a>

`./postProc` creates a new folder with the calculation results of the last saved iteration and 
new file `paraview.foam`. Download the reconstructed folder with calculated results, `constant` and `paraview.foam` 
from the HPC cluster to interactively examine the solution using the _ParaVIEW_. 


<img src="README_fig/resultsT.png" width="600">

`log.chtMultiRegionSimpleFoam` can be used to analyse the temperature convergecy of the case.

# Model Assumptions <a name="limit"></a>

- The flow velocity in the channel correspons to the laminar flow. If the end user would like to run a case with a turbulent flow, a new mesh study must be performed and turbulence models or LES, DNS solvers must be introduced.
- It is assumed that the temperature does not change the properties of the material during the calculation.

# References <a name="ref"></a>

1. S.J. Pety et al. Carbon fiber composites with 2d microvasular networks for battery cooling. _International Journal of Heat and Mass Transfer_, 115:513–522, 2017.
2. M.H.Y. Tan et al. Computational design of microvascular radiative cooling panels for nanosatellites. _Journal of Thermophysics and Heat Transfer_, 32(3):605–616, 2018.
3. A.W. Mutua et al. Towards development of nature-inspired thermoresponsive vascular composites: Analysis of polymeric composites. _Construction and Building Materials_, 259(3):120407, 2020.
4. K-H. Cho and M-H Kim. Fluid flow characteristics of vascularized channel networks. _Chemical Engineering Science_, 65:6270–6281, 2010.
5. S.J. Pety et al. Active cooling of microvascular composites for battery packaging. _Smart Mater. Struct._, 26:105004, 2017.
